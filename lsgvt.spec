Name:		lsgvt
Version:	0.2
Release:	0.1%{?dist}
Summary:	List Gluster Volume Toplogy

Group:		Applications/System
License:	GPLv3
URL:		https://github.com/fvzwieten/%{name}
Source0:	https://github.com/fvzwieten/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz
BuildRoot:	%(mktemp -ud %{_tmppath}/%{name}-%{version}-%{release}-XXXXXX)

BuildRequires:	python-devel

%description
This program shows a pretty graphical representation of a Gluster volume's
topology. It uses the fuse-<volname>.vol file for it's source. If you do not
give any parameters, it shows the topology for all the volumes. Otherwise it
will show the topology for the given space-separated list of volume names.


%prep
%setup -q


%build


%install
rm -rf %{buildroot}
install -D -m 755 lsgvt %{buildroot}/%{_bindir}/


%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc COPYING README.md
%{_bindir}/lsgvt


%changelog
* Thu Mar 20 2014 Niels de Vos <ndevos@redhat.com> - 0.2-0.1
- Initial packaging.
